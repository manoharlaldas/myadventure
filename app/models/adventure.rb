class Adventure < ActiveRecord::Base
  mount_uploader :video, VideoUploader
  belongs_to :user

  has_many :comments
  has_attached_file :image, :styles => { :small=> '50x50>',:medium => '300x300>', :thumb => '100x100>'}
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
end
