class AdventuresController < ApplicationController
  before_action :set_adventure, only: [:show, :edit, :update, :destroy]
  before_action :set_user, only: [:show, :edit, :update, :destroy, :index]
  before_action :authenticate_user!

  # GET /adventures
  # GET /adventures.json
  def index
    @adventures = current_user.adventures
  end

  # GET /adventures/1
  # GET /adventures/1.json
  def show

    # @adventure = @current_user.adventures(params[:user_id])
    # @adventure = Adventure.find(params[:user_id])

    @adventures = current_user.adventures.find(params[:id])

  end

  # GET /adventures/new
  def new
    @adventure = Adventure.new(params[:adventure])
  end


  # GET /adventures/1/edit
  def edit
    @adventure = Adventure.find(params[:id])
  end

  # POST /adventures
  # POST /adventures.json
  def create
    @adventure = Adventure.new(adventure_params)

    @adventure.user= current_user
    respond_to do |format|
      if @adventure.save
        format.html { redirect_to user_adventures_path, notice: 'Adventure was successfully created.' }

    
      else
        format.html { render :new }
        format.json { render json: @adventure.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /adventures/1
  # PATCH/PUT /adventures/1.json
  def update
    @adventure = Adventure.find(params[:id])
    respond_to do |format|
      if @adventure.update(adventure_params)

        flash[:notice] = 'Adventure was successfully updated.'
        format.html { redirect_to user_adventure_path }
        format.xml  { head :ok }
      else
        format.html { render :edit }
        format.json { render json: @adventure.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /adventures/1
  # DELETE /adventures/1.json
  def destroy
    @adventure.destroy
    respond_to do |format|
      format.html { redirect_to adventures_url, notice: 'Adventure was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_adventure
    @adventure = current_user.adventures.find(params[:id])
  end

  def set_user
    @user = current_user
  end
    
  # Never trust parameters from the scary internet, only allow the white list through.
  def adventure_params
    params.require(:adventure).permit(:place, :description, :image, :title, :video, :user_id, :body)
  end
 

end
