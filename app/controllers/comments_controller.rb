class CommentsController < ApplicationController
  before_filter :authenticate_user!
 
    def create
      @adventure = Adventure.find(params[:adventure_id])
      @comment = @adventure.comments.create(comment_params)
      if @comment.save
        flash[:notice] = "Comment has been created."
        redirect_to @adventure
      else
        flash.now[:danger] = "error"
      end
    end 

    def comment_params
      params.require(:comment).permit(:body, :email)
    end  
end
