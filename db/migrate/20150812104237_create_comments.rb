class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.integer :adventure_id
      t.integer :user_id
      t.text :body
      t.references :adventure

      t.timestamps null: false
    end
  end
end
