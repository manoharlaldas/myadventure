class CreateAdventures < ActiveRecord::Migration
  def change
    create_table :adventures do |t|
      t.string :place
      t.string :description
      t.string :title
      t.timestamps null: false
    end
  end
end
